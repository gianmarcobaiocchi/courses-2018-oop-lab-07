package it.unibo.oop.lab.nesting2;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class OneListAcceptable<T> implements Acceptable<T> {

	private final Set<T> sequence;
	
	public OneListAcceptable(final List<T> list) {
		super();
		this.sequence = new HashSet<>(list);
	}
	
	@Override
	public Acceptor<T> acceptor() {
		Acceptor<T> acceptor = new Acceptor<T>() {

			@Override
			public void accept(T newElement) throws ElementNotAcceptedException {
				final Iterator<T> iterator = sequence.iterator();
				while(iterator.hasNext()) {
					if(iterator.next() == newElement) {
						iterator.remove();
						return;
					}
				}
				throw new ElementNotAcceptedException(newElement);	
			}

			@Override
			public void end() throws EndNotAcceptedException {
				if(!sequence.isEmpty()) {
					throw new EndNotAcceptedException();
				}
			}
			
		};
		return acceptor;
	}

}
